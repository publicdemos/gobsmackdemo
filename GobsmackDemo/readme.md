# Gobsmack Demo App by Martin Hansen

## Description

Simple Demonstration of a white label registration form.

## To Run on emulator

IOS -- have xcode installed and a iPhone X / 11 emulator available. NB due to time constraints this
has not been optimised for smaller screens or had imports done for android.

clone repo.
run yarn in project root to download dependencies
cd ios && npx pod-install && cd .. to install pods.

  ```
  npx react-native run-ios
  ```

To test the mock endpoints for the register form there is a switch command that will select
either a success (default), Email Exists or 500 return value.

How to apply the brand variants.

-- make sure you are in the root of the project and the metro server is stopped.

run the following cmd line 

yarn variant-whitelabel

then run the app again with npx react-native run-ios to apply the standard white label theme.

to run the demo More Than theme run

yarn variant-MoreThan.

make sure the metro server isn't running before switching themes to prevent issues.

You can modify the themes individually - go to the variants folder in the root, then you will see the folder structure. Make some changes then rerun the variant theme to apply them to the app and test.

this is a very simplified demo and doesn't include scripted changes to the icons, splashscreens, API config or any other things that normally would be done with bash scripts.



