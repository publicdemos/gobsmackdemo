import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import LandingScreen from '../../features/LandingScreen/screens/';
import RegisterUser from '../../features/RegisterUser/screens/';
import RegisterSuccess from '../../features/RegisterSuccess/screens';
import GenericError from '../../features/GenericError/screens';

const Stack = createStackNavigator();

const RegisterUserStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="LandingScreen"
      component={LandingScreen}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="RegisterUser"
      component={RegisterUser}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="RegisterSuccess"
      component={RegisterSuccess}
      options={{headerShown: false}}
    />
     <Stack.Screen
      name="GenericError"
      component={GenericError}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

export default RegisterUserStack;
