import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from '../NavigationService';
import {StatusBar} from 'react-native';
import RegisterUserStack from '../stacks/RegisterUserStack';

const App: React.FC = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <StatusBar barStyle={'light-content'} />
      <RegisterUserStack />
    </NavigationContainer>
  );
};

export default App;
