export default async function MockRegistrationAPI(
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  acceptedLegalDocuments: boolean,
  mockResultType: string,
) {
  console.log(
    'request Registration Params',
    firstName,
    lastName,
    email,
    password,
    acceptedLegalDocuments,
    mockResultType,
  );
  const mockURL = `https://8201a214-b888-4d0c-a5ed-c5ceccd1ebfb.mock.pstmn.io/user/register?example=${mockResultType}`;

  return fetch(mockURL, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      email: email,
      emailConfirmation: email,
      password: password,
      passwordConfirmation: password,
      acceptedLegalDocuments: acceptedLegalDocuments,
    }),
  })
    .then(processResponse)
    .then((res) => {
      const {statusCode, data} = res;
      console.log('status Code', statusCode);
      console.log('data', data);
      return statusCode;
    })
    .catch((error) => {
      console.error(error);
      return {name: 'network error', description: ''};
    });
}

function processResponse(response: any) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then((res) => ({
    statusCode: res[0],
    data: res[1],
  }));
}
