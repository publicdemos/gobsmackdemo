/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import Navigator from './navigation/';

declare const global: {HermesInternal: null | {}};

const RootNavigation: React.FC = () => {
  return <Navigator />;
};

const App = () => {
  return (
    <>
      <RootNavigation />
    </>
  );
};

export default App;
