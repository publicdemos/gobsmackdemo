import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  pillButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    marginHorizontal: '10%',
    padding: 10,
  },
  pillButton: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: '100%',
    height: 40,
  },
  textStyle: {
    textAlign: 'center',
  },
});

export default styles;
