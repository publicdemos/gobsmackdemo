import {StyleSheet} from 'react-native';
import BrandColors from '../../branding/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BrandColors.base.COLOR_WHITE,
  },
  logoContainer: {
    width: '80%',
    alignContent: 'center',
    justifyContent: 'center',
  },
  brandLogo: {
    resizeMode: 'contain',
    alignContent: 'center',
    width: '100%',
  },
  landingTitleContainer: {
    marginVertical: 10,
    width: '80%',
    alignContent: 'center',
    justifyContent: 'center',
  },
  termsContainer: {
    width: '100%',
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    marginVertical: 20,
  },
  buttonContainer: {
    width: '100%',
    marginTop: 5,
    alignContent: 'center',
    justifyContent: 'center',
  },
  inputFormMargin: {marginBottom: 5, marginTop: 20},
  titleText: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 24,
    marginBottom: 10,
  },
  termsText: {
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 18,
    marginTop: 5,
    marginRight: 10,
  },
  errorText: {
    fontWeight: '700',
    color: BrandColors.base.COLOR_ERROR,
    fontSize: 13,
  },
  segmentedControl: {height: 20, width: '100%'},
  segmentedControlText: {fontSize: 10, color: BrandColors.palette.COLOR_GRAY},
  segmentedControlActiveText: {
    fontSize: 10,
    color: BrandColors.palette.COLOR_WHITE,
  },
});

export default styles;
