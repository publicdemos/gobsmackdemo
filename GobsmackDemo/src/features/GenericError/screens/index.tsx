import React from 'react';
import {SafeAreaView, Text, Image, View} from 'react-native';
import navigationService from '../../../navigation/NavigationService';

import styles from './styles';
import BrandColors from '../../branding/colors';

import PillButton from '../components/buttons/pillButton';

const GenericError: React.FC = () => {
  const home = () => navigationService.navigate('LandingScreen');

  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../branding/images/primaryBrandLogo.png')}
            style={styles.brandLogo}
          />
        </View>
        <View style={styles.landingTitleContainer}>
          <Text style={styles.landingTitleText}>
            Sorry, a Network Server Error Ocurred. Please try again later.
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <PillButton
            buttonLabel={'Return To Homescreen'}
            backgroundColor={BrandColors.base.COLOR_PRIMARY}
            textColor={BrandColors.base.COLOR_WHITE}
            onPress={home}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

export default GenericError;
