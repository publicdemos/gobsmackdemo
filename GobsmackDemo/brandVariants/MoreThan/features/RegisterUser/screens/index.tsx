import React, {useState} from 'react';
import {SafeAreaView, Text, Image, View, Keyboard} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import SegmentedControl from '@react-native-segmented-control/segmented-control';

import navigationService from '../../../navigation/NavigationService';

import styles from './styles';
import BrandColors from '../../branding/colors';

import PillButton from '../../../components/buttons/pillButton';
import ValidatedInputField from '../../../components/inputs/validatedInputField';

import MockRegistrationAPI from '../../../services/api/mocks/registration';

const RegisterUser: React.FC = () => {
  const cancel = () => navigationService.goBack();
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [validFirstName, setValidFirstName] = useState(false);
  const [validLastName, setValidLastName] = useState(false);
  const [validEmail, setValidEmail] = useState(false);
  const [validPassword, setValidPassword] = useState(false);
  const [acceptedLegalDocuments, setAcceptedLegalDocuments] = useState(false);
  const [emailErrorText, setEmailErrorText] = useState('');
  const [passwordErrorText, setPasswordErrorText] = useState('');
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [mockResultType, setMockResultType] = useState('success');
  const [
    acceptedLegalDocumentsErrorText,
    setAcceptedLegalDocumentsErrorText,
  ] = useState('');
  const handleFirstNameChange = (text: string) => {
    setFirstName(text);
  };

  const handleFirstNameOnBlur = () => {
    if (firstName.length < 1) {
      setValidFirstName(false);
    } else {
      setValidFirstName(true);
    }
    Keyboard.dismiss();
  };

  const handleLastNameChange = (text: string) => {
    setLastName(text);
  };

  const handleLastNameOnBlur = () => {
    if (lastName.length < 1) {
      setValidLastName(false);
    } else {
      setValidLastName(true);
    }
    Keyboard.dismiss();
  };

  const handleEmailChange = (text: string) => {
    setEmail(text);
    /* eslint-disable no-useless-escape */
    const emailCheckRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    /* eslint-enable no-useless-escape */
    if (emailCheckRegex.test(text)) {
      setValidEmail(true);
      setEmailErrorText('');
    } else {
      setValidEmail(false);
    }
  };
  const handleEmailOnBlur = () => {
    //this should set the email in the redux store
  };

  const handlePasswordChange = (text: string) => {
    const passwordCheckRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
    setPassword(text);
    if (passwordCheckRegex.test(text)) {
      setValidPassword(true);
      setPasswordErrorText('');
    } else {
      setValidPassword(false);
    }
  };
  const handlePasswordOnBlur = () => {
    //dispatch to redux here
  };

  const testMock = async () => {
    //in a production app we'd call this conditionally when in dev or test configuration only
    const result = await MockRegistrationAPI(
      firstName,
      lastName,
      email,
      password,
      acceptedLegalDocuments,
      mockResultType,
    );
    console.log('server result was', result);
    handleAPIResponse(result);
  };

  const handleAPIResponse = (apiResponse: any) => {
    switch (apiResponse) {
      case 200:
        //success
        navigationService.navigate('RegisterSuccess');
        break;
      case 400:
        //user email exists
        setValidEmail(false);
        setEmailErrorText('Sorry, that email already exists');
        break;
      case 500:
        navigationService.navigate('GenericError');
        break;
      default:
        break;
    }
  };

  const checkForm = () => {
    if (!validEmail) {
      setEmailErrorText('Please enter a valid email');
    }
    if (!validPassword) {
      setPasswordErrorText(
        'Please enter a valid password that is 8 chars long and contains 1 symbol, 1 number, 1 uppercase and 1 lowercase',
      );
    }
    if (!acceptedLegalDocuments) {
      setAcceptedLegalDocumentsErrorText('Please accept terms to continue');
    }

    if (validEmail && validPassword) {
      //call to submit form to redux dispatch will normally go here
      //for testing we'll just call the fetch mock function
      testMock();
    }
  };

  const onSetAPIResultChange = (setResultType: number) => {
    if (setResultType === 0) {
      setSelectedIndex(0);
      setMockResultType('success');
    }
    if (setResultType === 1) {
      setSelectedIndex(1);
      setMockResultType('emailExists');
    }
    if (setResultType === 2) {
      setSelectedIndex(2);
      setMockResultType('500');
    }
  };

  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../branding/images/primaryBrandLogo.png')}
            style={styles.brandLogo}
          />
        </View>
        <View style={styles.landingTitleContainer}>
          <Text style={styles.titleText}>Account Registration</Text>
          <ValidatedInputField
            labelText={'First Name'}
            labelTextSize={14}
            labelColor={BrandColors.palette.COLOR_CHARCOAL}
            textColor={BrandColors.palette.COLOR_BLACK}
            borderBottomColor={BrandColors.palette.COLOR_SILVER}
            inputType="text"
            customStyle={styles.inputFormMargin}
            onChangeText={(text: any) => handleFirstNameChange(text)}
            onBlur={() => handleFirstNameOnBlur()}
            showCheckmark={validFirstName}
            autoFocus={true}
            autoCapitalize={'none'}
            multiline={false}
            numberOfLines={1}
            textAlignVertical={'top'}
            selectionColor={BrandColors.base.COLOR_PRIMARY}
            textValue={firstName}
          />
          <ValidatedInputField
            labelText={'Last Name'}
            labelTextSize={14}
            labelColor={BrandColors.palette.COLOR_CHARCOAL}
            textColor={BrandColors.palette.COLOR_BLACK}
            borderBottomColor={BrandColors.palette.COLOR_SILVER}
            inputType="text"
            customStyle={styles.inputFormMargin}
            onChangeText={(text: any) => handleLastNameChange(text)}
            onBlur={() => handleLastNameOnBlur()}
            showCheckmark={validLastName}
            autoFocus={true}
            autoCapitalize={'none'}
            multiline={false}
            numberOfLines={1}
            textAlignVertical={'top'}
            selectionColor={BrandColors.base.COLOR_PRIMARY}
            textValue={lastName}
          />
          <ValidatedInputField
            labelText={'Email'}
            labelTextSize={14}
            labelColor={BrandColors.palette.COLOR_CHARCOAL}
            textColor={BrandColors.base.COLOR_BLACK}
            borderBottomColor={BrandColors.palette.COLOR_SILVER}
            inputType="email"
            customStyle={styles.inputFormMargin}
            onChangeText={(text: any) => handleEmailChange(text)}
            onBlur={() => handleEmailOnBlur()}
            showCheckmark={validEmail}
            autoFocus={true}
            autoCapitalize={'none'}
            multiline={false}
            numberOfLines={1}
            textAlignVertical={'top'}
            selectionColor={BrandColors.base.COLOR_PRIMARY}
            textValue={email}
          />
          <Text style={styles.errorText}>{emailErrorText}</Text>
          <ValidatedInputField
            labelText={'Password'}
            labelTextSize={14}
            labelColor={BrandColors.palette.COLOR_CHARCOAL}
            textColor={BrandColors.base.COLOR_BLACK}
            borderBottomColor={BrandColors.palette.COLOR_SILVER}
            inputType="password"
            customStyle={styles.inputFormMargin}
            onChangeText={(text: any) => handlePasswordChange(text)}
            onBlur={() => handlePasswordOnBlur()}
            showCheckmark={validPassword}
            autoFocus={false}
            autoCapitalize={'none'}
            multiline={false}
            numberOfLines={1}
            textAlignVertical={'top'}
            selectionColor={BrandColors.base.COLOR_PRIMARY}
            textValue={password}
          />
          <Text style={styles.errorText}>{passwordErrorText}</Text>
          <View style={styles.termsContainer}>
            <Text style={styles.termsText}>
              I agree to the terms of service
            </Text>
            <CheckBox
              disabled={false}
              value={acceptedLegalDocuments}
              onValueChange={(newValue) => setAcceptedLegalDocuments(newValue)}
              onTintColor={BrandColors.base.COLOR_PRIMARY}
              onCheckColor={BrandColors.base.COLOR_PRIMARY}
            />
          </View>
          <Text style={styles.errorText}>
            {acceptedLegalDocumentsErrorText}
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <PillButton
            buttonLabel={'Register'}
            backgroundColor={BrandColors.base.COLOR_PRIMARY}
            textColor={BrandColors.base.COLOR_WHITE}
            onPress={checkForm}
          />
          <PillButton
            buttonLabel={'cancel'}
            backgroundColor={BrandColors.base.COLOR_WHITE}
            textColor={BrandColors.base.COLOR_PRIMARY}
            onPress={cancel}
          />
        </View>
        <View style={styles.segmentedControl}>
          <SegmentedControl
            values={['Success', 'User Exists', 'Server Error']}
            tintColor={BrandColors.base.COLOR_PRIMARY}
            style={styles.segmentedControl}
            fontStyle={styles.segmentedControlText}
            activeFontStyle={styles.segmentedControlActiveText}
            selectedIndex={selectedIndex}
            onChange={(event) => {
              onSetAPIResultChange(event.nativeEvent.selectedSegmentIndex);
            }}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

export default RegisterUser;
