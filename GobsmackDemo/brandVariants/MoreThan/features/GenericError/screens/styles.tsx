import {StyleSheet} from 'react-native';
import BrandColors from '../../branding/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BrandColors.base.COLOR_WHITE,
  },
  logoContainer: {
    width: '80%',
    alignContent: 'center',
    justifyContent: 'center',
  },
  brandLogo: {
    resizeMode: 'contain',
    alignContent: 'center',
    width: '100%',
  },
  landingTitleContainer: {
    marginTop: 100,
    width: '80%',
    alignContent: 'center',
    justifyContent: 'center',
  },
  landingTitleText: {
    textAlign: 'center',
  },
  buttonContainer: {
    width: '100%',
    marginTop: 50,
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export default styles;
