const BrandColors = {
  base: {
    COLOR_PRIMARY: '#30A400',
    COLOR_SECONDARY: '#111',
    COLOR_ERROR: '#E74C3C',
    COLOR_WHITE: '#FFFFFF',
    COLOR_BLACK: '#000000',
    COLOR_PLACEHOLDER: '#111111',
    COLOR_GREY_WHITE: '#fafafa',
    COLOR_DARK_SEPERATOR: '#d4d4d4',
    COLOR_BLACK_TRANSP: 'rgba(0, 0, 0, 0.7)',
    COLOR_GREY_TRANSP: 'rgba(67, 85, 85, 0.7)',
  },
  palette: {
    COLOR_POWDER_BLUE: '#298CFF',
    COLOR_APPLE_GREEN: '#30A400',
    COLOR_BLOOD_ORANGE: '#FF5B32',
    COLOR_JOKER_PURPLE: '#8A39FF',
    COLOR_LIGHT_PURPLE: '#744BAC',
    COLOR_FIRE_ORANGE: '#FF6A00',
    COLOR_ROSSO_CORSA: '#E74C3C',
    COLOR_BLACK: '#000000',
    COLOR_WHITE: '#FFFFFF',
    COLOR_SILVER: '#C0C0C0',
    COLOR_GRAY: '#808080',
    COLOR_CHARCOAL: '#36454f',
  },
};
export default BrandColors;
