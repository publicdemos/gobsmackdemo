import React from 'react';
import {SafeAreaView, Text, Image, View} from 'react-native';
import navigationService from '../../../navigation/NavigationService';

import styles from './styles';
import BrandColors from '../../branding/colors';

import PillButton from '../components/buttons/pillButton';

const RegisterSuccess: React.FC = () => {
  const login = () => navigationService.navigate('LandingScreen');

  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../branding/images/primaryBrandLogo.png')}
            style={styles.brandLogo}
          />
        </View>
        <View style={styles.landingTitleContainer}>
          <Text style={styles.landingTitleText}>
            Congratulations. Your Account was registered. You can now login.
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <PillButton
            buttonLabel={'Login'}
            backgroundColor={BrandColors.base.COLOR_PRIMARY}
            textColor={BrandColors.base.COLOR_WHITE}
            onPress={login}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

export default RegisterSuccess;
