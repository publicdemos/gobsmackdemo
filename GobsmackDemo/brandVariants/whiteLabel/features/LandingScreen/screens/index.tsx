import React from 'react';
import {SafeAreaView, Text, Image, View} from 'react-native';
import navigationService from '../../../navigation/NavigationService';

import styles from './styles';
import BrandColors from '../../branding/colors';

import PillButton from '../components/buttons/pillButton';

const LandingScreen: React.FC = () => {
  const register = () => navigationService.navigate('RegisterUser');

  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../branding/images/primaryBrandLogo.png')}
            style={styles.brandLogo}
          />
        </View>
        <View style={styles.landingTitleContainer}>
          <Text style={styles.landingTitleText}>
            Gobsmack White Label App Demo
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <PillButton
            buttonLabel={'register'}
            backgroundColor={BrandColors.base.COLOR_PRIMARY}
            textColor={BrandColors.base.COLOR_WHITE}
            onPress={register}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

export default LandingScreen;
