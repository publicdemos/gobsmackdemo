import React from 'react';
import {View, Text, TouchableHighlight} from 'react-native';
import styles from './styles';

interface IPillButtonProps {
  buttonLabel: string;
  onPress?: () => void;
  backgroundColor: string;
  textColor: string;
}

const PillButton: React.FC<IPillButtonProps> = ({
  buttonLabel,
  backgroundColor,
  textColor,
  onPress,
}: IPillButtonProps) => {
  return (
    <View style={styles.pillButtonContainer}>
      <TouchableHighlight
        style={[{backgroundColor: backgroundColor}, styles.pillButton]}
        onPress={onPress}>
        <Text style={[{color: textColor}, styles.textStyle]}>
          {buttonLabel}
        </Text>
      </TouchableHighlight>
    </View>
  );
};

export default PillButton;
